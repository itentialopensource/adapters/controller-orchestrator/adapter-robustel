
## 0.2.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-robustel!1

---

## 0.1.1 [03-16-2022]

- Initial Commit

See commit 645342b

---
