
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:09PM

See merge request itentialopensource/adapters/adapter-robustel!11

---

## 0.4.3 [09-16-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-robustel!9

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:17PM

See merge request itentialopensource/adapters/adapter-robustel!8

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_19:30PM

See merge request itentialopensource/adapters/adapter-robustel!7

---

## 0.4.0 [07-10-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-robustel!6

---

## 0.3.3 [03-28-2024]

* Changes made at 2024.03.28_13:37PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-robustel!5

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_15:56PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-robustel!4

---

## 0.3.1 [02-28-2024]

* Changes made at 2024.02.28_11:24AM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-robustel!3

---

## 0.3.0 [12-29-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/controller-orchestrator/adapter-robustel!2

---

## 0.2.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-robustel!1

---

## 0.1.1 [03-16-2022]

- Initial Commit

See commit 645342b

---
