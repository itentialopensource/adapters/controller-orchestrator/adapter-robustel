# Robustel

Vendor: Robustel
Homepage: https://www.robustel.com/

Product: Robustel
Product Page: https://www.robustel.com/

## Introduction
We classify Robustel into the Data Center and Network Services domaina as Robustel provides a Controller/Orchestrator solution for the Internet of Things.

"Robustel is committed to helping businesses and industries across the world continue to solve their IoT & M2M problems."

## Why Integrate
The Robustel adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Robustel. With this adapter you have the ability to perform operations with Robustel on items such as:

- Device
- Command
- Dashboard

## Additional Product Documentation
The [Robustel Cloud Manage Service Overview](https://www.robustel.com/wp-content/uploads/dlm_uploads/2019/07/RCMS-Brochure-2.pdf)